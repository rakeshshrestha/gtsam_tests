#!/usr/bin/env python

import sys
from numpy import genfromtxt
import matplotlib.pyplot as plt

if (len(sys.argv) < 2) or sys.argv[1] in ['-h', '--help']:
	print('Usage: python plot_error.py <output_file>')
	exit(0)

data = genfromtxt(sys.argv[1], delimiter=',')

ax = plt.subplot(2, 3, 1)
ax.plot(data[:, 0], label='actual')
ax.plot(data[:, 6], label='predicted')
ax.set_xlabel("Frame id")
ax.set_ylabel("Relative Motion")
ax.set_title("t_x")
plt.legend(shadow=True, fancybox=True)

ax = plt.subplot(2, 3, 2)
ax.plot(data[:, 1], label='actual')
ax.plot(data[:, 7], label='predicted')
ax.set_xlabel("Frame id")
ax.set_ylabel("Relative Motion")
ax.set_title("t_y")
plt.legend(shadow=True, fancybox=True)

ax = plt.subplot(2, 3, 3)
ax.plot(data[:, 2], label='actual')
ax.plot(data[:, 8], label='predicted')
ax.set_xlabel("Frame id")
ax.set_ylabel("Relative Motion")
ax.set_title("t_z")
plt.legend(shadow=True, fancybox=True)

ax = plt.subplot(2, 3, 4)
ax.plot(data[:, 3], label='actual')
ax.plot(data[:, 9], label='predicted')
ax.set_xlabel("Frame id")
ax.set_ylabel("Relative Motion")
ax.set_title("roll")
plt.legend(shadow=True, fancybox=True)

ax = plt.subplot(2, 3, 5)
ax.plot(data[:, 4], label='actual')
ax.plot(data[:, 10], label='predicted')
ax.set_xlabel("Frame id")
ax.set_ylabel("Relative Motion")
ax.set_title("pitch")
plt.legend(shadow=True, fancybox=True)

ax = plt.subplot(2, 3, 6)
ax.plot(data[:, 5], label='actual')
ax.plot(data[:, 11], label='predicted')
ax.set_xlabel("Frame id")
ax.set_ylabel("Relative Motion")
ax.set_title("yaw")
plt.legend(shadow=True, fancybox=True)

plt.show()