#include <gtsam/nonlinear/NonlinearFactor.h>
#include <gtsam/geometry/Point2.h>
#include <gtsam/nonlinear/GaussNewtonOptimizer.h>
#include <gtsam/nonlinear/LevenbergMarquardtOptimizer.h>
#include <gtsam/nonlinear/Marginals.h>

using namespace gtsam;

/*
 * @brief: optmizes the equation 1/2(a*x1 + b*x2 + c)^2
 */
class HelloworldFactor: public NoiseModelFactor1<Point2> 
{
protected:
	double a_;
	double b_;
	double c_;
public:
	typedef NoiseModelFactor1<Point2> Base;
	HelloworldFactor(	Key j,
						double a, double b, double c,
                    	const SharedNoiseModel& model
                    )
	: Base(model, j), a_(a), b_(b), c_(c)
	{

	}

    virtual ~HelloworldFactor() {}

    Vector evaluateError(const Point2& point,
                         boost::optional<Matrix&> H = boost::none) const
    {
    	if (H)
    	{
    		*H = (Vector2() << a_, b_).finished().transpose();
    	}
    	return (Vector1() << a_ * point(0) + b_ * point(1) + c_).finished();
    }

    gtsam::NonlinearFactor::shared_ptr clone() const 
    {
        return boost::static_pointer_cast<gtsam::NonlinearFactor>(
                gtsam::NonlinearFactor::shared_ptr(new HelloworldFactor(*this)));
    }
};

int main(int arc, char **argv)
{
	NonlinearFactorGraph graph;
	gtsam::SharedNoiseModel pointNoise = noiseModel::Diagonal::Variances(
			(gtsam::Vector(1) << 1).finished()
	);
	graph.add(boost::make_shared<HelloworldFactor>(
		0, 
		-1.0, 2.0, 10.0, // a, b, c
		pointNoise
	));

	Values initial_values;
	initial_values.insert(0, Point2(5.0, 7.0));
	LevenbergMarquardtParams optimizer_params;
	optimizer_params.setVerbosity("ERROR");
	LevenbergMarquardtOptimizer optimizer(graph, initial_values, optimizer_params);
	gtsam::Values result = optimizer.optimize();

	graph.print("Factor Graph:\n");
	result.print("Final results:\n");
	std::cout << "initial error = " << graph.error(initial_values) << std::endl;
	std::cout << "final error = " << graph.error(result) << std::endl;

	// 5. Calculate and print marginal covariances for all variables
	gtsam::Marginals marginals(graph, result);
	std::cout << "x1 covariance:\n" << marginals.marginalCovariance(0) << std::endl;

}