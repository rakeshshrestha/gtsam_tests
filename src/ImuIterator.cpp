/**
 *
 * Class ImuIterator
 * Data structure for getting IMU readings
 *
 */

#include "ImuIterator.h"

ImuIterator::ImuIterator(const std::string file_path):
    mIsStashed(false)
{

  mFileStream.open(file_path);

  // ignore the first line
  std::string tmp_string;
  std::getline(mFileStream, tmp_string);

}

ImuIterator::~ImuIterator()
{
    mFileStream.close();
}

ImuIterator::imu_measurement_t ImuIterator::next()
{

  if (mIsStashed) {
    mIsStashed = false;

    // Don't read new IMU measurement, give the previous one
    return mStashedMeasurement;
  }

  std::string tmp_string;
  std::stringstream ss;  

  if (mFileStream.good()) {
    
    std::getline(mFileStream, tmp_string, ',');

    if (tmp_string.empty()) {

      mStashedMeasurement.timestamp = 0;
      
    } else {
      
      ss.clear();
      ss << tmp_string;
      ss >> mStashedMeasurement.timestamp;

      Eigen::Matrix<double,6,1> imu = Eigen::Matrix<double,6,1>::Zero();
      
      for (int i=0; i<5; ++i) {
        
        std::getline(mFileStream, tmp_string, ',');
        
        ss.clear();
        ss << tmp_string;
        ss >> imu(i);

      }
      
      std::getline(mFileStream, tmp_string, '\n');

      ss.clear();
      ss << tmp_string;
      ss >> imu(5);
      
      mStashedMeasurement.gyro = imu.head<3>();
      mStashedMeasurement.accel = imu.tail<3>();

    }

  } else {

    mStashedMeasurement.timestamp = 0;

  }

  return mStashedMeasurement;
}