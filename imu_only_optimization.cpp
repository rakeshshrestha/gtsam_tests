/* ----------------------------------------------------------------------------

 * GTSAM Copyright 2010, Georgia Tech Research Corporation,
 * Atlanta, Georgia 30332-0415
 * All Rights Reserved
 * Authors: Frank Dellaert, et al. (see THANKS for the full author list)

 * See LICENSE for the license information

 * -------------------------------------------------------------------------- */

/**
 * @file imuFactorsExample
 * @brief Test example for using GTSAM ImuFactor and ImuCombinedFactor navigation code.
 * @author Garrett (ghemann@gmail.com), Luca Carlone
 */

/**
 * Example of use of the imuFactors (imuFactor and combinedImuFactor) in conjunction with GPS
 *  - you can test imuFactor (resp. combinedImuFactor) by commenting (resp. uncommenting)
 *  the line #define USE_COMBINED (few lines below)
 *  - we read IMU and GPS data from a CSV file, with the following format:
 *  A row starting with "i" is the first initial position formatted with
 *  N, E, D, qx, qY, qZ, qW, velN, velE, velD
 *  A row starting with "0" is an imu measurement
 *  linAccN, linAccE, linAccD, angVelN, angVelE, angVelD
 *  A row starting with "1" is a gps correction formatted with
 *  N, E, D, qX, qY, qZ, qW
 *  Note that for GPS correction, we're only using the position not the rotation. The
 *  rotation is provided in the file for ground truth comparison.
 */

// GTSAM related includes.
#include <gtsam/navigation/CombinedImuFactor.h>
#include <gtsam/navigation/GPSFactor.h>
#include <gtsam/navigation/ImuFactor.h>
#include <gtsam/slam/dataset.h>
#include <gtsam/slam/BetweenFactor.h>
#include <gtsam/slam/PriorFactor.h>
#include <gtsam/nonlinear/LevenbergMarquardtOptimizer.h>
#include <gtsam/nonlinear/NonlinearFactorGraph.h>
#include <gtsam/inference/Symbol.h>
#include <fstream>
#include <iostream>

// Uncomment line below to use the CombinedIMUFactor as opposed to the standard ImuFactor.
#define USE_COMBINED

using namespace gtsam;
using namespace std;

using symbol_shorthand::X; // Pose3 (x,y,z,r,p,y)
using symbol_shorthand::V; // Vel   (xdot,ydot,zdot)
using symbol_shorthand::B; // Bias  (ax,ay,az,gx,gy,gz)

const string output_filename = "imuFactorExampleResults.csv";

// This will either be PreintegratedImuMeasurements (for ImuFactor) or
// PreintegratedCombinedMeasurements (for CombinedImuFactor).
PreintegrationType *imu_preintegrated_;

int main(int argc, char* argv[])
{
  string data_filename;
  if (argc < 2) {
    printf("using default CSV file\n");
    data_filename = findExampleDataFile("imuAndGPSdata.csv");
  } else {
    data_filename = argv[1];
  }

  // Set up output file for plotting errors
  FILE* fp_out = fopen(output_filename.c_str(), "w+");
  fprintf(fp_out, "#time(s),x(m),y(m),z(m),qx,qy,qz,qw,gt_x(m),gt_y(m),gt_z(m),gt_qx,gt_qy,gt_qz,gt_qw\n");

  // Begin parsing the CSV file.  Input the first line for initialization.
  // From there, we'll iterate through the file and we'll preintegrate the IMU
  // or add in the GPS given the input.
  ifstream file(data_filename.c_str());
  string value;

  // Format is (N,E,D,qX,qY,qZ,qW,velN,velE,velD)
  Eigen::Matrix<double,10,1> initial_state = Eigen::Matrix<double,10,1>::Zero();
  getline(file, value, ','); // i
  for (int i=0; i<9; i++) {
    getline(file, value, ',');
    initial_state(i) = atof(value.c_str());
  }
  getline(file, value, '\n');
  initial_state(9) = atof(value.c_str());
  cout << "initial state:\n" << initial_state.transpose() << "\n\n";

  // Assemble initial quaternion through gtsam constructor ::quaternion(w,x,y,z);
  Rot3 prior_rotation = Rot3::Quaternion(initial_state(6), initial_state(3), 
                                         initial_state(4), initial_state(5));
  Point3 prior_point(initial_state.head<3>());
  Pose3 prior_pose(prior_rotation, prior_point);
  Vector3 prior_velocity(initial_state.tail<3>());
  imuBias::ConstantBias prior_imu_bias; // assume zero initial bias

  // Assemble prior noise model and add it the graph.
  noiseModel::Diagonal::shared_ptr pose_noise_model = noiseModel::Diagonal::Sigmas((Vector(6) << 0.01, 0.01, 0.01, 0.5, 0.5, 0.5).finished()); // rad,rad,rad,m, m, m
  noiseModel::Diagonal::shared_ptr velocity_noise_model = noiseModel::Isotropic::Sigma(3,0.1); // m/s
  noiseModel::Diagonal::shared_ptr bias_noise_model = noiseModel::Isotropic::Sigma(6,1e-3);

  // We use the sensor specs to build the noise model for the IMU factor.
  double accel_noise_sigma = 0.0003924;
  double gyro_noise_sigma = 0.000205689024915;
  double accel_bias_rw_sigma = 0.004905;
  double gyro_bias_rw_sigma = 0.000001454441043;
  Matrix33 measured_acc_cov = Matrix33::Identity(3,3) * pow(accel_noise_sigma,2);
  Matrix33 measured_omega_cov = Matrix33::Identity(3,3) * pow(gyro_noise_sigma,2);
  Matrix33 integration_error_cov = Matrix33::Identity(3,3)*1e-8; // error committed in integrating position from velocities
  Matrix33 bias_acc_cov = Matrix33::Identity(3,3) * pow(accel_bias_rw_sigma,2);
  Matrix33 bias_omega_cov = Matrix33::Identity(3,3) * pow(gyro_bias_rw_sigma,2);
  Matrix66 bias_acc_omega_int = Matrix::Identity(6,6)*1e-5; // error in the bias used for preintegration

  boost::shared_ptr<PreintegratedCombinedMeasurements::Params> p = PreintegratedCombinedMeasurements::Params::MakeSharedD(0.0);
  // PreintegrationBase params:
  p->accelerometerCovariance = measured_acc_cov; // acc white noise in continuous
  p->integrationCovariance = integration_error_cov; // integration uncertainty continuous
  // should be using 2nd order integration
  // PreintegratedRotation params:
  p->gyroscopeCovariance = measured_omega_cov; // gyro white noise in continuous
  // PreintegrationCombinedMeasurements params:
  p->biasAccCovariance = bias_acc_cov; // acc bias in continuous
  p->biasOmegaCovariance = bias_omega_cov; // gyro bias in continuous
  p->biasAccOmegaInt = bias_acc_omega_int;
  
#ifdef USE_COMBINED
  imu_preintegrated_ = new PreintegratedCombinedMeasurements(p, prior_imu_bias);
#else
  imu_preintegrated_ = new PreintegratedImuMeasurements(p, prior_imu_bias);
#endif

  // Store previous state for the imu integration and the latest predicted outcome.
  NavState prev_state(prior_pose, prior_velocity);
  NavState prop_state = prev_state;
  imuBias::ConstantBias prev_bias = prior_imu_bias;

  double output_time = 0.0;
  double dt = 0.005;  // The real system has noise, but here, results are nearly 
                      // exactly the same, so keeping this for simplicity.

  // All priors have been set up, now iterate through the data file.
  while (file.good()) {

    // Parse out first value
    getline(file, value, ',');
    int type = atoi(value.c_str());

    if (type == 0) { // IMU measurement
      Eigen::Matrix<double,6,1> imu = Eigen::Matrix<double,6,1>::Zero();
      for (int i=0; i<5; ++i) {
        getline(file, value, ',');
        imu(i) = atof(value.c_str());
      }
      getline(file, value, '\n');
      imu(5) = atof(value.c_str());

      // Adding the IMU preintegration.
      imu_preintegrated_->integrateMeasurement(imu.head<3>(), imu.tail<3>(), dt);

    } else if (type == 1) { // GPS measurement
      Eigen::Matrix<double,7,1> gps = Eigen::Matrix<double,7,1>::Zero();
      for (int i=0; i<6; ++i) {
        getline(file, value, ',');
        gps(i) = atof(value.c_str());
      }
      getline(file, value, '\n');
      gps(6) = atof(value.c_str());

      // Add all prior factors (pose, velocity, bias) to the graph.
      NonlinearFactorGraph *graph = new NonlinearFactorGraph();
      Values initial_values;

      // Adding IMU factor and GPS factor and optimizing.
#ifdef USE_COMBINED
      PreintegratedCombinedMeasurements *preint_imu_combined = dynamic_cast<PreintegratedCombinedMeasurements*>(imu_preintegrated_);
      CombinedImuFactor imu_factor(X(0), V(0),
                                   X(1), V(1),
                                   B(0), B(1),
                                   *preint_imu_combined);
      graph->add(imu_factor);
#else
      PreintegratedImuMeasurements *preint_imu = dynamic_cast<PreintegratedImuMeasurements*>(imu_preintegrated_);
      ImuFactor imu_factor(X(0), V(0),
                           X(1), V(1),
                           B(0),
                           *preint_imu);
      graph->add(imu_factor);
      imuBias::ConstantBias zero_bias(Vector3(0, 0, 0), Vector3(0, 0, 0));
      graph->add(BetweenFactor<imuBias::ConstantBias>(B(0), 
                                                      B(1), 
                                                      zero_bias, bias_noise_model));
#endif

      // Now optimize and compare results.
      prop_state = imu_preintegrated_->predict(prev_state, prev_bias);
      
      const double noise = -0.25;
      const double noise1 = 0.25;

      NavState prev_state_noisy = NavState(
        prev_state.pose().compose(
          Pose3::Expmap(
            (Vector6() << noise1, noise, noise, noise1, noise, noise).finished()
          )
        ),
        prev_state.velocity() + (Vector3() << noise1, noise, noise1).finished()
      );

      NavState prop_state_noisy = NavState(
        prop_state.pose().compose(
          Pose3::Expmap(
            (Vector6() << noise1, noise, noise1, noise, noise, noise).finished()
          )
        ),
        prop_state.velocity() + (Vector3() << noise, noise1, noise).finished()
      );

      // initial_values.insert(X(1), prop_state_noisy.pose());
      // initial_values.insert(V(1), prop_state_noisy.v());
      // initial_values.insert(B(1), prev_bias);

      // initial_values.insert(X(0), prev_state.pose());
      // initial_values.insert(V(0), prev_state.v());
      // initial_values.insert(B(0), prev_bias);

      // LevenbergMarquardtParams lm_params;
      // lm_params.setVerbosityLM("DELTA");
      // lm_params.setVerbosity("ERROR");
      
      // LevenbergMarquardtOptimizer optimizer(*graph, initial_values, lm_params);
      // Values result = optimizer.optimize();

      // Overwrite the beginning of the preintegration for the next step.
      // prev_state = NavState(result.at<Pose3>(X(1)),
      //                       result.at<Vector3>(V(1)));
      // prev_bias = result.at<imuBias::ConstantBias>(B(1));

      delete graph;

      // ----------------- Optimizing manually ----------------- //
      Matrix J, b;
      NavState current_state = prop_state_noisy;
      NavState previous_state = prev_state_noisy;
      for (size_t iter_idx = 0; iter_idx < 8; iter_idx++)
      {
        Values values;
        values.insert(X(1), current_state.pose());
        values.insert(V(1), current_state.v());
        values.insert(B(1), prev_bias);

        values.insert(X(0), previous_state.pose());
        values.insert(V(0), previous_state.v());
        values.insert(B(0), prev_bias);

        boost::shared_ptr<GaussianFactor> gaussian_factor = imu_factor.linearize(values);

        Matrix J_Rt_i, J_v_i, J_Rt_j, J_v_j, J_bias_i, J_bias_j;
        

#ifdef USE_COMBINED
        Eigen::Matrix<double, 15, 9> J_i;
        Eigen::Matrix<double, 15, 9> J_j;
        
        Matrix information = dynamic_cast<gtsam::PreintegratedCombinedMeasurements*>(imu_preintegrated_)->preintMeasCov().inverse();

        Eigen::Matrix<double, 15, 1> error = imu_factor.evaluateError(
          prev_state.pose(),
          prev_state.velocity(),
          current_state.pose(),
          current_state.velocity(),
          prev_bias, prev_bias,
          J_Rt_i, J_v_i, J_Rt_j, J_v_j, J_bias_i, J_bias_j
        );
#else
        Eigen::Matrix<double, 9, 9> J_i;
        Eigen::Matrix<double, 9, 9> J_j;
        
        Matrix information = dynamic_cast<gtsam::PreintegratedImuMeasurements*>(imu_preintegrated_)->preintMeasCov().inverse();
        
        Eigen::Matrix<double, 9, 1> error = imu_factor.evaluateError(
          prev_state.pose(),
          prev_state.velocity(),
          current_state.pose(),
          current_state.velocity(),
          prev_bias,
          J_Rt_i, J_v_i, J_Rt_j, J_v_j, J_bias_i, J_bias_j
        );
#endif

        // Eigen::Matrix<double, 6, 1> inc_Rt_i = (J_Rt_i.transpose() * information.topLeftCorner<6, 6>() * J_Rt_i).ldlt().solve(
        //   -J_Rt_i.transpose() * information.topLeftCorner<6, 6>() * error.head(6)
        // );

        // Eigen::Matrix<double, 3, 1> inc_v_j = (J_v_j.transpose() * information * J_v_j).ldlt().solve(
        //   -J_v_j.transpose() * information * error
        // );

        // Eigen::Matrix<double, 6, 1> inc_Rt_j = (J_Rt_j.transpose() * information * J_Rt_j).ldlt().solve(
        //   -J_Rt_j.transpose() * information * error
        // );

        // std::tie(J, b) = gtsam::JacobianFactor(*gaussian_factor).jacobianUnweighted();
        // std::cout << "J: \n" << J << std::endl; 
        // Eigen::Matrix<double, 24, 1> inc = (J.transpose() * J).ldlt().solve(J.transpose() * b);
        
        // current_state = NavState(
        //   current_state.pose().compose(Pose3::Expmap(inc.segment<6>(9))),
        //   current_state.velocity() + inc.segment<3>(15)
        // );

        // std::cout << "error: " << -b.transpose() << std::endl;
        // std::cout << "Inc_i: " << inc.head(9).transpose() << std::endl;
        // std::cout << "Inc_j: " << inc.segment<9>(9).transpose() << std::endl;

        Eigen::Matrix<double, 18, 18> H;
        Eigen::Matrix<double, 18, 1> b;

        Eigen::Matrix<double, 6, 1> inc_Rt_i;
        Eigen::Matrix<double, 3, 1> inc_v_i;

        Eigen::Matrix<double, 6, 1> inc_Rt_j;
        Eigen::Matrix<double, 3, 1> inc_v_j;

        H.setZero();
        b.setZero();

        // ----------------------- increment previous state ----------------------- //
        J_i.setZero();
        J_i.leftCols(6) = J_Rt_i;
        J_i.rightCols(3) = J_v_i;


        H.topLeftCorner<9, 9>() += J_i.transpose() * information * J_i;
        b.head(9) += J_i.transpose() * information * error;
        
        // ----------------------- increment current state ----------------------- //
        J_j.setZero();
        J_j.leftCols(6) = J_Rt_j;
        J_j.rightCols(3) = J_v_j;

        H.bottomRightCorner<9, 9>() += J_j.transpose() * information * J_j;
        b.tail(9) += J_j.transpose() * information * error;

        // solve from combined hessian
        Eigen::Matrix<double, 18, 1> inc_ij = H.ldlt().solve(-b);
        inc_Rt_i = inc_ij.head(6);
        inc_v_i = inc_ij.segment<3>(6);
        inc_Rt_j = inc_ij.segment<6>(9);
        inc_v_j = inc_ij.tail(3);

        previous_state = NavState(
          previous_state.pose().compose(Pose3::Expmap(inc_Rt_i)),
          previous_state.velocity() + inc_v_i
        );

        current_state = NavState(
          current_state.pose().compose(Pose3::Expmap(inc_Rt_j)),
          current_state.velocity() + inc_v_j
        );

        std::cout << "error: " << error.transpose() << std::endl;
        std::cout << "inc_Rt_i: " << inc_Rt_i.transpose() << std::endl;
        std::cout << "inc_v_i: " << inc_v_i.transpose() << std::endl;
        std::cout << "inc_Rt_j: " << inc_Rt_j.transpose() << std::endl;
        std::cout << "inc_v_j: " << inc_v_j.transpose() << std::endl;


        std::cout << std::endl << std::endl;
      }

      // Reset the preintegration object.
      imu_preintegrated_->resetIntegrationAndSetBias(prev_bias);

      std::cout << std::endl << std::endl << std::endl;

      output_time += 1.0;

    } else {
      cerr << "ERROR parsing file\n";
      return 1;
    }
  }
  fclose(fp_out);
  cout << "Complete, results written to " << output_filename << "\n\n";;
  return 0;
}