/**
 *
 * Class GroundTruthIterator
 * Data structure for getting ground truth readings
 *
 */

#include <GroundTruthIterator.h>

GroundTruthIterator::GroundTruthIterator(const std::string file_path):
  mIsStashed(false)
{
  
  mFileStream.open(file_path);

  // ignore the first line
  std::string tmp_string;
  std::getline(mFileStream, tmp_string);

}

GroundTruthIterator::~GroundTruthIterator()
{
  mFileStream.close();
}


GroundTruthIterator::ground_truth_measurement_t GroundTruthIterator::next()
{

  if (mIsStashed) {
    mIsStashed = false;

    // Don't read new IMU measurement, give the previous one
    return mStashedMeasurement;
  }

  std::string tmp_string;
  std::stringstream ss;  

  if (mFileStream.good()) {
    
    std::getline(mFileStream, tmp_string, ',');

    if (tmp_string.empty()) {

      mStashedMeasurement.timestamp = 0;
      
    } else {
      
      ss.clear();
      ss << tmp_string;
      ss >> mStashedMeasurement.timestamp;

      Eigen::Matrix<double,NO_OF_READING_IN_GROUND_TRUTH,1> ground_truth = Eigen::Matrix<double,NO_OF_READING_IN_GROUND_TRUTH,1>::Zero();
      
      for (int i=0; i<(NO_OF_READING_IN_GROUND_TRUTH-1); ++i) {
        
        std::getline(mFileStream, tmp_string, ',');
        
        ss.clear();
        ss << tmp_string;
        ss >> ground_truth(i);

      }
      
      std::getline(mFileStream, tmp_string, '\n');

      ss.clear();
      ss << tmp_string;
      ss >> ground_truth(NO_OF_READING_IN_GROUND_TRUTH-1);
      
      mStashedMeasurement.pose = gtsam::Pose3 (
                                                                  
                                                gtsam::Rot3::quaternion(ground_truth(3), ground_truth(4), ground_truth(5), ground_truth(6)), 
                                                gtsam::Point3(ground_truth(0), ground_truth(1), ground_truth(2))
                                              
                                              );

      if (ground_truth.size() == 16) {
        mStashedMeasurement.velocity = gtsam::Velocity3 (
                                                          ground_truth(7), ground_truth(8), ground_truth(9)
                                                        );
      }
      

    }

  } else {
  
    mStashedMeasurement.timestamp = 0;
  
  }

  return mStashedMeasurement;
}

