/**
 * @author Rakesh Shrestha (rakeshs@sfu.ca)
 *
 * Generates motion priors for consecutive frames using IMU reading
 * Compares the prior with actual motion using poses from ground truth file
 * Ground truth file and imu file are of ASL MAV dataset format
 * 
 * Command-line arguments: <camera file> <imu file> <ground truth file>
 */

// GTSAM related includes.
#include <gtsam/navigation/CombinedImuFactor.h>
#include <gtsam/navigation/GPSFactor.h>
#include <gtsam/navigation/ImuFactor.h>
#include <gtsam/slam/dataset.h>
#include <gtsam/slam/BetweenFactor.h>
#include <gtsam/slam/PriorFactor.h>
#include <gtsam/nonlinear/LevenbergMarquardtOptimizer.h>
#include <gtsam/nonlinear/NonlinearFactorGraph.h>
#include <gtsam/inference/Symbol.h>

// In-Project includes
#include "ImuIterator.h"
#include "GroundTruthIterator.h"

#include <stdlib.h>
#include <ctime>
#include <chrono>
#include <ctime>
#include <random>
#include <fstream>
#include <iostream>

// No of reading in ground truth file (not including the timestamp)
// can be either 7 (x,y,z,qw,qx,qy,qz) like VICON or 16 (+ v_x,v_y,v_z,bg_x,bg_y,bg_z,ba_x,ba_y,ba_z) like the state estimate
#define NO_OF_READING_IN_GROUND_TRUTH 16

// Standard deviations of noise on ground-truth
#define TRANSLATION_NOISE_STD 0.0
#define ROTATION_NOISE_STD 0.05
#define VELOCITY_NOISE_STD 0.0 // 0.05

#define GRAVITY_W gtsam::Vector3(0, 0, -9.81) // gtsam::Vector3(-0.426633, 9.2043056, 3.3667254)
#define BIAS_ACCELERO gtsam::Vector3(-0.013337, 0.103464, 0.093086)
#define BIAS_GYRO gtsam::Vector3(-0.002153, 0.020744, 0.075806)

// Uncomment line below to use the CombinedIMUFactor as opposed to the standard ImuFactor.
// #define USE_COMBINED

using namespace gtsam;

using symbol_shorthand::X; // Pose3 (x,y,z,r,p,y)
using symbol_shorthand::V; // Vel   (xdot,ydot,zdot)
using symbol_shorthand::B; // Bias  (ax,ay,az,gx,gy,gz)

// This will either be PreintegratedImuMeasurements (for ImuFactor) or
// PreintegratedCombinedMeasurements (for CombinedImuFactor).
PreintegrationType *imu_preintegrated_;

int main(int argc, char* argv[])
{
  
  if (argc < 4) {
    
    std::cout << "Usage: imu_motion_prior <camera file> <imu file> <ground truth file>" << std::endl;
    return 0;
  
  }

  // ---------------------------- Parameters start ---------------------------- //
  
  // We use the sensor specs to build the noise model for the IMU factor.
  double accel_noise_sigma = 0.0003924;
  double gyro_noise_sigma = 0.000205689024915;
  double accel_bias_rw_sigma = 0.004905;
  double gyro_bias_rw_sigma = 0.000001454441043;
  Matrix33 measured_acc_cov = Matrix33::Identity(3,3) * pow(accel_noise_sigma,2);
  Matrix33 measured_omega_cov = Matrix33::Identity(3,3) * pow(gyro_noise_sigma,2);
  Matrix33 integration_error_cov = Matrix33::Identity(3,3)*1e-8; // error committed in integrating position from velocities
  Matrix33 bias_acc_cov = Matrix33::Identity(3,3) * pow(accel_bias_rw_sigma,2);
  Matrix33 bias_omega_cov = Matrix33::Identity(3,3) * pow(gyro_bias_rw_sigma,2);
  Matrix66 bias_acc_omega_int = Matrix::Identity(6,6)*1e-5; // error in the bias used for preintegration

  boost::shared_ptr<PreintegratedCombinedMeasurements::Params> p = PreintegratedCombinedMeasurements::Params::MakeSharedD(0.0);
  // PreintegrationBase params:
  p->accelerometerCovariance = measured_acc_cov; // acc white noise in continuous
  p->integrationCovariance = integration_error_cov; // integration uncertainty continuous
  // should be using 2nd order integration
  // PreintegratedRotation params:
  p->gyroscopeCovariance = measured_omega_cov; // gyro white noise in continuous
  // PreintegrationCombinedMeasurements params:
  p->biasAccCovariance = bias_acc_cov; // acc bias in continuous
  p->biasOmegaCovariance = bias_omega_cov; // gyro bias in continuous
  p->biasAccOmegaInt = bias_acc_omega_int;
  // gravity vector
  p->n_gravity = GRAVITY_W;

  imuBias::ConstantBias bias_prior(   
                                    BIAS_ACCELERO,
                                    BIAS_GYRO
                                  );


  // ---------------------------- Parameters end ---------------------------- //

  // Noise generator
  unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
  std::default_random_engine random_number_generator(seed);

  std::normal_distribution<double> t_x_noise (0.0, TRANSLATION_NOISE_STD);
  std::normal_distribution<double> t_y_noise (0.0, TRANSLATION_NOISE_STD);
  std::normal_distribution<double> t_z_noise (0.0, TRANSLATION_NOISE_STD);

  std::normal_distribution<double> roll_noise (0.0, ROTATION_NOISE_STD);
  std::normal_distribution<double> pitch_noise (0.0, ROTATION_NOISE_STD);
  std::normal_distribution<double> yaw_noise (0.0, ROTATION_NOISE_STD);

  std::normal_distribution<double> velocity_x_noise(0.0, VELOCITY_NOISE_STD);
  std::normal_distribution<double> velocity_y_noise(0.0, VELOCITY_NOISE_STD);
  std::normal_distribution<double> velocity_z_noise(0.0, VELOCITY_NOISE_STD);

  // tracked time
  double total_preintegration_time = 0;
  unsigned int total_preintegrations = 0;
  
  // read the command-line arguments
  std::ifstream camera_file(argv[1]);
  

  if (camera_file.good()) {
    
    // skip the first reading
    std::string tmp_string;
    std::getline(camera_file, tmp_string);
  
  } else {
    
    std::cerr << "Invalid image file" << std::endl;
  
  }

  std::time_t current_camera_timestamp = 0;
  std::time_t previous_camera_timestamp = 0;

  // Iterators for imu and ground truth data files
  ImuIterator imu_iterator( (const std::string)std::string(argv[2]) );
  GroundTruthIterator ground_truth_iterator( (const std::string)std::string(argv[3]) );

#ifdef USE_COMBINED
  imu_preintegrated_ = new PreintegratedCombinedMeasurements(p, bias_prior);
#else
  imu_preintegrated_ = new PreintegratedImuMeasurements(p, bias_prior);
#endif
  
  while (camera_file.good()) {
    
    std::string tmp_string;
    std::stringstream ss;
    
    // read the camera timestamp
    std::getline(camera_file, tmp_string, ',');

    if (tmp_string.empty()) {
      break;
    }

    ss << tmp_string;
    ss >> current_camera_timestamp;

    if (previous_camera_timestamp) {
      
      // std::cout << "Camera Timestamp between " 
      //           << previous_camera_timestamp << " - " 
      //           << current_camera_timestamp 
      //           << " = " << (current_camera_timestamp - previous_camera_timestamp) / 1e6 << " ms"
      //           << std::endl;

      // predicted velocity of camera (starts with rest)
      gtsam::Velocity3 predicted_velocity(0., 0., 0.);
      
      gtsam::Pose3 predicted_relative_motion;
      gtsam::Pose3 actual_relative_motion;

      std::time_t previous_imu_timestamp = 0;
      
      // ----------------------------------------------------------------------------------------------- //
      // Preintegrate the IMU timestamp between previous_camera_timestamp and current_camera_timestamp
      while (true) {

        ImuIterator::imu_measurement_t imu_measurement;
        bool is_last_imu_reading = false;
      
        // time elapsed between measurements
        std::time_t dt = 0;

        imu_measurement = imu_iterator.next();

        if (!imu_measurement.timestamp) {

          // No IMU measurements
          break;

        } else if (imu_measurement.timestamp >= previous_camera_timestamp && imu_measurement.timestamp <= current_camera_timestamp) {

          if (previous_imu_timestamp) {
            
            dt = imu_measurement.timestamp - previous_imu_timestamp;

          } else {

            // prorate the dt to previous_camera_timestamp (so that you'll get relative motion between camera measurements and not IMU measurements)
            dt = imu_measurement.timestamp - previous_camera_timestamp;

          }

          previous_imu_timestamp = imu_measurement.timestamp;

          // std::cout << dt << std::endl;
        
        } else if (imu_measurement.timestamp > current_camera_timestamp) {
          
          // prorate the  dt to current_camera_timestamp (so that you'll get relative motion between camera measurements and not IMU measurements)
          if (current_camera_timestamp > previous_imu_timestamp) {
            
            dt = current_camera_timestamp - previous_imu_timestamp;
            imu_preintegrated_->integrateMeasurement( 
                                                      imu_measurement.accel,
                                                      imu_measurement.gyro,
                                                      dt * 1e-9 // timestamp is in ns (we need seconds)
                                                    );

            // std::cout << dt << std::endl;

          }

          // since the timestamp of imu measurement read is greater that current camera timestamp, ...
          // this imu measurement will be useful for the next camera frame. 
          imu_iterator.stash();

          is_last_imu_reading = true;
        
        }

        if (dt) {
          std::chrono::steady_clock::time_point t1 = std::chrono::steady_clock::now();
          imu_preintegrated_->integrateMeasurement( 
                                                    imu_measurement.accel,
                                                    imu_measurement.gyro,
                                                    dt * 1e-9 // timestamp is in ns (we need seconds)
                                                  );
          std::chrono::steady_clock::time_point t2 = std::chrono::steady_clock::now();

          double ttrack = std::chrono::duration_cast<std::chrono::duration<double> >(t2 - t1).count();
          total_preintegration_time += ttrack;
          total_preintegrations++;
        }

        if (is_last_imu_reading) {
          break;
        }

      }

      // ----------------------------- Find relative motion from ground truth ----------------------------- //
      GroundTruthIterator::ground_truth_measurement_t cam1_pose;
      GroundTruthIterator::ground_truth_measurement_t cam2_pose;

      bool is_first_ground_truth = true;
      GroundTruthIterator::ground_truth_measurement_t previous_ground_truth;
      

      while (true) {
                  
        GroundTruthIterator::ground_truth_measurement_t ground_truth = ground_truth_iterator.next();  

        if (ground_truth.timestamp == 0) {
          
          break;

        } else if (ground_truth.timestamp >= previous_camera_timestamp && ground_truth.timestamp <= current_camera_timestamp) {

          if (is_first_ground_truth) {

            is_first_ground_truth = false;
            cam1_pose = ground_truth;



            // debug
            // std::cout << "Ground Truth: " << cam1_pose.timestamp;

          }

          previous_ground_truth = ground_truth;

        } else if (ground_truth.timestamp > current_camera_timestamp) {

          ground_truth_iterator.stash();
          cam2_pose = previous_ground_truth;

          actual_relative_motion = cam1_pose.pose.between(cam2_pose.pose);

          // debug
          // std::cout << " - " << cam2_pose.timestamp;
          // std::cout << " = " << (cam2_pose.timestamp - cam1_pose.timestamp) / 1e6 << " ms" << std::endl;

          break;
        }
      }

      if (is_first_ground_truth) {
        // No ground truth
        std::cerr << "No ground truth between " << previous_camera_timestamp << " - " << current_camera_timestamp << std::endl;
      
      } else {
        // add noise to the ground truth state of cam1 (something that would happen on real SLAM)
        gtsam::Pose3 cam1_pose_noisy(cam1_pose.pose.rotation().compose (
                                                                          Rot3::ypr (
                                                                                      yaw_noise(random_number_generator),
                                                                                      pitch_noise(random_number_generator),
                                                                                      roll_noise(random_number_generator)
                                                                                    )
                                                                      ),


                                    cam1_pose.pose.translation() + Point3 (
                                                                            t_x_noise(random_number_generator),
                                                                            t_y_noise(random_number_generator),
                                                                            t_z_noise(random_number_generator)
                                                                          )
                                  );

        gtsam::Velocity3 cam1_velocity_noisy = cam1_pose.velocity + Velocity3 (
                                                                                velocity_x_noise(random_number_generator),
                                                                                velocity_y_noise(random_number_generator),
                                                                                velocity_z_noise(random_number_generator)
                                                                              );

        // Predict the relative motion between the two camera frames
        gtsam::NavState predicted_absolute_state = imu_preintegrated_->predict(
                                                        NavState(
                                                          cam1_pose_noisy.rotation(), 
                                                          cam1_pose_noisy.translation(),
                                                          cam1_velocity_noisy // predicted_velocity // 
                                                        ),

                                                        bias_prior
                                                      );

        gtsam::Rot3 relativeDebug = cam1_pose_noisy.rotation().inverse().compose(predicted_absolute_state.pose().rotation());
        gtsam::Vector3 logDebug = gtsam::Rot3::Logmap(relativeDebug);
        std::cout << logDebug.transpose() << std::endl;

        
        predicted_relative_motion = cam1_pose_noisy.between(predicted_absolute_state.pose());
        predicted_velocity = predicted_absolute_state.velocity();
        
        // prorate the groundtruth motion according to time (TODO: this is naive, use previous and future position to interpolate)
        Point3 actual_translation_prorated = actual_relative_motion.translation() * ((double)current_camera_timestamp - previous_camera_timestamp) / ((cam2_pose.timestamp - cam1_pose.timestamp));
        
        // prorate relative angles (TODO: this is naive, use previous and future angles to interpolate)
        Vector3 angular_motion_ypr = actual_relative_motion.rotation().ypr() * ((double)current_camera_timestamp - previous_camera_timestamp) / ((cam2_pose.timestamp - cam1_pose.timestamp));
        Rot3 actual_angular_motion_prorated = Rot3::ypr(
                                                  angular_motion_ypr(2),
                                                  angular_motion_ypr(1),
                                                  angular_motion_ypr(0)
                                                );

        // Print out the position and orientation error for comparison.
        // Vector3 position_error =  predicted_relative_motion.translation() - actual_translation_prorated;
        // double current_position_error = position_error.norm();

        
        // Quaternion quat_error = predicted_relative_motion.rotation().toQuaternion() * 
        //                         actual_angular_motion_prorated.toQuaternion().inverse();

        // quat_error.normalize();
        // Vector3 euler_angle_error(
        //                             quat_error.x()*2,
        //                             quat_error.y()*2,
        //                             quat_error.z()*2
        //                           );

        // double current_orientation_error = euler_angle_error.norm();

        // display statistics

        // std::cout << actual_translation_prorated.x() << ","
        //           << actual_translation_prorated.y() << ","
        //           << actual_translation_prorated.z() << ","
        //           << euler_angle_error(0) << ","
        //           << euler_angle_error(1) << ","
        //           << euler_angle_error(2) << std::endl;

        // std::cout << "---------------- Predicted: ----------------" << std::endl;
        // std::cout << predicted_relative_motion.translation() << std::endl;

        // std::cout << "---------------- Actual: ----------------" << std::endl;
        // std::cout << actual_relative_motion.translation() << std::endl;

        // std::cout << "Position error:" << current_position_error << "\t " << "Angular error:" << current_orientation_error << "\n";

        // std:: cout << "-----------------------------------" << std::endl;
        // std::cout << std::endl << std::endl;

        // Actual Vs Error
        // std::cout << actual_translation_prorated.x() << "," << actual_translation_prorated.y() << "," << actual_translation_prorated.z() << ","
        //           << actual_angular_motion_prorated.toQuaternion().x() * 2 << "," << actual_angular_motion_prorated.toQuaternion().y() * 2 << "," << actual_angular_motion_prorated.toQuaternion().z() * 2 << ","
        //           << position_error(0) << "," << position_error(1) << "," << position_error(2) << ","
        //           << euler_angle_error(0) << "," << euler_angle_error(1) << "," << euler_angle_error(2) << std::endl;

        // Actual Vs Predicted
        // TODO: the predicted pitch is comming exactly negative of actual. Find out!!!
        // std::cout << actual_translation_prorated.x() << "," << actual_translation_prorated.y() << "," << actual_translation_prorated.z() << ","
        //           << actual_angular_motion_prorated.rpy()(0) << "," << actual_angular_motion_prorated.rpy()(1) << "," << actual_angular_motion_prorated.rpy()(2) << ","
        //           << predicted_relative_motion.translation().x() << "," << predicted_relative_motion.translation().y() << "," << predicted_relative_motion.translation().z() << ","
        //           << predicted_relative_motion.rotation().rpy()(0) << "," << predicted_relative_motion.rotation().rpy()(1) << "," << predicted_relative_motion.rotation().rpy()(2) << std::endl;
      }

      // Reset the preintegration object.
      // Since we're not doing any optimization, our bias estimate never changes (which isn't good for prior generation!!!)
      imu_preintegrated_->resetIntegrationAndSetBias(bias_prior);

    }

    previous_camera_timestamp = current_camera_timestamp;

    std::getline(camera_file, tmp_string);
  }

  std::clog << "Average preintegration times: " << total_preintegration_time /*/ total_preintegrations*/ << std::endl;
  return 0;
}