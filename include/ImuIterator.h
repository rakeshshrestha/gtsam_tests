/**
 *
 * Class ImuIterator
 * Data structure for getting IMU readings
 *
 */

#ifndef IMU_ITERATOR_H

#include <stdlib.h>
#include <ctime>
#include <chrono>
#include <ctime>
#include <random>
#include <fstream>
#include <iostream>

// GTSAM related includes.
#include <gtsam/navigation/CombinedImuFactor.h>
#include <gtsam/navigation/GPSFactor.h>
#include <gtsam/navigation/ImuFactor.h>
#include <gtsam/slam/dataset.h>
#include <gtsam/slam/BetweenFactor.h>
#include <gtsam/slam/PriorFactor.h>
#include <gtsam/nonlinear/LevenbergMarquardtOptimizer.h>
#include <gtsam/nonlinear/NonlinearFactorGraph.h>
#include <gtsam/inference/Symbol.h>


class ImuIterator
{
public:
  typedef struct {
    
    std::time_t timestamp;
    gtsam::Vector3 accel;
    gtsam::Vector3 gyro;

  } imu_measurement_t;

  ImuIterator(const std::string file_path);

  ~ImuIterator();


  ImuIterator::imu_measurement_t next();
  
  void stash() { mIsStashed = true; }


protected:
  std::ifstream mFileStream;
  bool mIsStashed;
  imu_measurement_t mStashedMeasurement;
};

#endif