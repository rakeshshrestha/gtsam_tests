// GTSAM related includes.
#include <gtsam/base/numericalDerivative.h>
#include <gtsam/base/OptionalJacobian.h>
#include <gtsam/navigation/NavState.h>
#include <fstream>
#include <iostream>
#include <cstdlib>
#include <ctime>

#define ROT_NOISE 0.5
#define TRANSLATION_NOISE 0.5
#define VELOCITY_NOISE 0.5

#define INFORMATION_R 10
#define INFORMATION_T 10
#define INFORMATION_V 10

using namespace gtsam;

namespace gtsam
{

class PriorFactor
{
protected:
    NavState navstate0_;

    /// @{
    /// serialization
    friend class boost::serialization::access;
    template<class ARCHIVE>
    void serialize(ARCHIVE & ar, const unsigned int /*version*/) {
        ar & BOOST_SERIALIZATION_NVP(navstate0_);
    }
    /// @}
public:
    PriorFactor(NavState navstate0) : navstate0_(navstate0)
    {

    }
    
    Vector9 calcPriorRes(const NavState navstate1, OptionalJacobian<9, 9> J_prior = boost::none) const
    {
        Vector9 res_prior;
        res_prior.setZero();

        Matrix3 D_dR_R, D_dt_R, D_dv_R;
        const Rot3 dR = navstate1.pose().rotation().between(
                navstate0_.pose().rotation(),
                &D_dR_R
        );
        Point3 dt = navstate1.pose().rotation().unrotate(
                navstate0_.pose().translation() - navstate1.pose().translation(),
                &D_dt_R
        );
        Vector dv = navstate1.pose().rotation().unrotate(
                navstate0_.velocity() - navstate1.velocity(),
                &D_dv_R
        );

        Vector9 xi;
        Matrix3 D_xi_R;

        res_prior.head(3) = Rot3::Logmap(dR, &D_xi_R);
        res_prior.segment<3>(3) = dt.vector();
        res_prior.segment<3>(6) = dv;

        if (J_prior)
        {
            (*J_prior).setZero();
            // diagonals
            (*J_prior).topLeftCorner<3, 3>() = D_xi_R * D_dR_R;
            (*J_prior).block<3, 3>(3, 3) = -Matrix3::Identity();
            (*J_prior).block<3, 3>(6, 6) = -Matrix3::Identity();//navstate1.R().transpose(); // Matrix3::Identity();
            // off-diagonals
            // t, R
            (*J_prior).block<3, 3>(3, 0) = D_dt_R;
            // v, R
            (*J_prior).block<3, 3>(6, 0) = D_dv_R;
        }
        // Separate out derivatives
        // Note that doing so requires special treatment of velocities, as when treated as
        // separate variables the retract applied will not be the semi-direct product in NavState
        // Instead, the velocities in nav are updated using a straight addition
        // This is difference is accounted for by the R().transpose calls below
        // J_prior.rightCols<3>().noalias() = J_prior.rightCols<3>() * navstate1.R().transpose();
        // TODO: bias error

        return res_prior;
    }
};


} // namespace gtsam

int main(int argc, char **argv)
{
    srand(time(0));

    // Prior
    Rot3 R0 = Rot3::ypr(
        (rand()/(double)RAND_MAX - 0.5) * 180,
        (rand()/(double)RAND_MAX - 0.5) * 180,
        (rand()/(double)RAND_MAX - 0.5) * 180
    );

    Vector3 t0 = (Vector3() << 
        (rand()/(double)RAND_MAX - 0.5) * 100,
        (rand()/(double)RAND_MAX - 0.5) * 100,
        (rand()/(double)RAND_MAX - 0.5) * 100
    ).finished();

    Vector3 v0 = (Vector3() <<
        (rand()/(double)RAND_MAX - 0.5) * 4,
        (rand()/(double)RAND_MAX - 0.5) * 4,
        (rand()/(double)RAND_MAX - 0.5) * 4
    ).finished();

    NavState navstate0 = NavState(R0, t0, v0);

    // estimate
    Rot3 R1 = R0.compose(
        Rot3::Expmap( 
            (Vector3() <<  
                (rand()/(double)RAND_MAX - 0.5) * ROT_NOISE,
                (rand()/(double)RAND_MAX - 0.5) * ROT_NOISE,
                (rand()/(double)RAND_MAX - 0.5) * ROT_NOISE
            ).finished()
        )
    );

    Vector3 t1 = t0 + (Vector3() <<  
        (rand()/(double)RAND_MAX - 0.5) * TRANSLATION_NOISE,
        (rand()/(double)RAND_MAX - 0.5) * TRANSLATION_NOISE,
        (rand()/(double)RAND_MAX - 0.5) * TRANSLATION_NOISE
    ).finished();

    Vector3 v1 = v0 + (Vector3() <<  
        (rand()/(double)RAND_MAX - 0.5) * VELOCITY_NOISE,
        (rand()/(double)RAND_MAX - 0.5) * VELOCITY_NOISE,
        (rand()/(double)RAND_MAX - 0.5) * VELOCITY_NOISE
    ).finished();

    NavState navstate1(R1, t1, v1);

    PriorFactor prior_factor(navstate0);

    for (size_t i = 0; i < 8; i++)
    {
        Matrix9 J;
        Vector9 res = prior_factor.calcPriorRes(navstate1, &J);

        boost::function<Vector9(const NavState&)> boostCalcRes = boost::bind(&PriorFactor::calcPriorRes, prior_factor, _1, boost::none);
        Matrix9 J_numerical = numericalDerivative11(
            boostCalcRes,
            navstate1
        );

        Matrix9 information_matrix;
        information_matrix.setZero();
        information_matrix.diagonal() = (Vector9() <<
            INFORMATION_T, INFORMATION_T, INFORMATION_T,
            INFORMATION_R, INFORMATION_R, INFORMATION_R,
            INFORMATION_V, INFORMATION_V, INFORMATION_V
        ).finished();
        information_matrix.topLeftCorner(3, 3) = (Vector3() << INFORMATION_T, INFORMATION_T, INFORMATION_T).finished().asDiagonal();
        information_matrix.block<3, 3>(3, 3) = (Vector3() << INFORMATION_R, INFORMATION_R, INFORMATION_R).finished().asDiagonal();
        information_matrix.block<3, 3>(6, 6) = (Vector3() << INFORMATION_V, INFORMATION_V, INFORMATION_V).finished().asDiagonal();

        Matrix9 H = J.transpose() * information_matrix * J;
        Vector9 b = J.transpose() * information_matrix * res;

        // std::cout   << "J: \n" << J << std::endl
        //             << "H: \n" << H << std::endl
        //             << "b: " << b.transpose() << std::endl
        //             << "info: \n" << information_matrix << std::endl; 
           

        Vector9 increment = H.ldlt().solve(-b);
        Vector6 pose_increment;
        // pose_increment << increment.segment<3>(3), increment.head(3);
        pose_increment = increment.head(6);

        navstate1 = NavState(
            navstate1.pose().compose(
                Pose3::Expmap(
                    pose_increment                    
                )
            ),
            navstate1.velocity() + navstate1.pose().rotation().matrix() * increment.tail(3)
        );

        std::cout << "J_numerical: \n" << J_numerical << std::endl;
        std::cout << "J_analytic: \n" << J << std::endl << std::endl;
        std::cout << "Increment: " << increment.transpose() << std::endl;
        std::cout << "Error: " << res.transpose() << std::endl;
        std::cout << std::endl << std::endl;
    }

    return 0;
}
